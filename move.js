
var scale = 0.65;
var mscale = "matrix(" + scale + ", 0, 0," + scale + ",";
function transformScale(x, y) {
  return mscale + x + "," + y + ")";
}

var svgpad;
var page = {};
var keypad;
var keyboard;
var keylist = {
  "tilda" : [
    { text : "`", size : "18", x : "22", y : "40", anchor : "start" },
    { text : "~", size : "15", x : "10", y : "20", anchor : "start" },
  ],
  "one" : [
    { text : "1", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "two" : [
    { text : "2", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "three" : [
    { text : "3", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "four"  : [
    { text : "4", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "five"  : [
    { text : "5", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "six" : [
    { text : "6", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "seven" : [
    { text : "7", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "eight" : [
    { text : "8", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "nine"  : [
    { text : "9", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "zero"  : [
    { text : "0", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "minus" : [
    { text : "-", size : "18", x : "20", y : "35", anchor : "start" },
    { text : "_", size : "15", x : "10", y : "10", anchor : "start" },
  ],
  "equal" : [
    { text : "=", size : "18", x : "25", y : "35", anchor : "start" },
    { text : "+", size : "15", x : "10", y : "20", anchor : "start" },
  ],
  "bspace"  : [
    { text : "backspace", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "tab" : [
    { text : "tab", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "Q" : [
    { text : "Q", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "W" : [
    { text : "W", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "E" : [
    { text : "E", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "R" : [
    { text : "R", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "T" : [
    { text : "T", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "Y" : [
    { text : "Y", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "U" : [
    { text : "U", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "I" : [
    { text : "I", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "O" : [
    { text : "O", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "P" : [
    { text : "P", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "lsqbr" : [
    { text : "[", size : "18", x : "25", y : "35", anchor : "start" },
    { text : "{", size : "15", x : "10", y : "20", anchor : "start" },
  ],
  "rsqbr" : [
    { text : "]", size : "18", x : "25", y : "35", anchor : "start" },
    { text : "}", size : "15", x : "10", y : "20", anchor : "start" },
  ],
  "bslash" : [
    { text : "\\", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "clock" : [
    { text : "caps lock", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "A" : [
    { text : "A", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "S" : [
    { text : "S", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "D" : [
    { text : "D", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "F" : [
    { text : "F", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "G" : [
    { text : "G", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "H" : [
    { text : "H", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "J" : [
    { text : "J", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "K" : [
    { text : "K", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "L" : [
    { text : "L", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "semicol" : [
    { text : ";", size : "18", x : "25", y : "35", anchor : "start" },
    { text : ":", size : "18", x : "10", y : "20", anchor : "start" },
  ],
  "singleq" : [
    { text : "'", size : "18", x : "22", y : "37", anchor : "start" },
    { text : "\"", size : "18", x : "10", y : "20", anchor : "start" },
  ],
  "enter" : [
    { text : "enter  &#8629;", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "lshift" : [
    { text : "l shift", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "Z" : [
    { text : "Z", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "X" : [
    { text : "X", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "C" : [
    { text : "C", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "V" : [
    { text : "V", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "B" : [
    { text : "B", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "N" : [
    { text : "N", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "M" : [
    { text : "M", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "comma" : [
    { text : ",", size : "18", x : "25", y : "35", anchor : "start" },
    { text : "<", size : "18", x : "10", y : "20", anchor : "start" },
  ],
  "dot" : [
    { text : ".", size : "18", x : "25", y : "35", anchor : "start" },
    { text : ">", size : "18", x : "10", y : "20", anchor : "start" },
  ],
  "slash" : [
    { text : "/", size : "18", x : "25", y : "35", anchor : "start" },
    { text : "?", size : "15", x : "10", y : "20", anchor : "start" },
  ],
  "rshift" : [
    { text : "r shift", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "lctrl" : [
    { text : "l ctrl", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "lwin" : [
    { text : "l win", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "alt" : [
    { text : "alt", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "space" : [
    { text : "", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "altgr" : [
    { text : "alt gr", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "rwin" : [
    { text : "r win", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "option" : [
    { text : "option", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "rctrl" : [
    { text : "r ctrl", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "esc" : [
    { text : "esc", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "f1" : [
    { text : "f1", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f2" : [
    { text : "f2", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f3" : [
    { text : "f3", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f4": [
    { text : "f4", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f5" : [
    { text : "f5", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f6" : [
    { text : "f6", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f7" : [
    { text : "f7", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f8" : [
    { text : "f8", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f9" : [
    { text : "f9", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f10" : [
    { text : "f10", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f11" : [
    { text : "f11", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "f12" : [
    { text : "f12", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "prtsc" : [
    { text : "pr sc", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "scrlk" : [
    { text : "sc lk", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "pause" : [
    { text : "pause", size : "15", x : "6", y : "30", anchor : "start" },
  ],
  "insert" : [
    { text : "ins", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "delete" : [
    { text : "del", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "home" : [
    { text : "home", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "end" : [
    { text : "end", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "pgup" : [
    { text : "pg up", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "pgdown" : [
    { text : "pg dn", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "up" : [
    { text : "&uarr;", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "down" : [
    { text : "&darr;", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "left" : [
    { text : "&larr;", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "right" : [
    { text : "&rarr;", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "numlk" : [
    { text : "num", size : "15", x : "10", y : "30", anchor : "start" },
  ],
  "num_slash" : [
    { text : "/", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "num_star" : [
    { text : "*", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "num_minus" : [
    { text : "-", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "num_plus" : [
    { text : "+", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "num_enter" : [
    { text : "&#8629;", size : "18", x : "10", y : "30", anchor : "start" },
  ],
  "num_comma" : [
    { text : ",", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "del", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_0" : [
    { text : "0", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "ins", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_1" : [
    { text : "1", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "end", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_2" : [
    { text : "2", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "&darr;", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_3" : [
    { text : "3", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "pg dn", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_4" : [
    { text : "4", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "&larr;", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_5" : [
    { text : "5", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "_", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_6" : [
    { text : "6", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "&rarr;", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_7" : [
    { text : "7", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "home", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_8" : [
    { text : "8", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "&uarr;", size : "15", x : "", y : "40", anchor : "middle" },
  ],
  "num_9" : [
    { text : "9", size : "18", x : "", y : "20", anchor : "middle" },
    { text : "pg up", size : "15", x : "", y : "40", anchor : "middle" },
  ],
};

var draggingElement = null;
var deltax = null;
var deltay = null;

function randomInt(min, max) {
  return Math.floor(Math.random()*(max + 1 - min) + min);
}

function mouseDown(evt) {
  evt.preventDefault();
  var target = evt.currentTarget;
  draggingElement = target;
  if(target) {
    deltax = parseFloat(target.getAttribute("elemx"), 10) - evt.clientX;
    deltay = parseFloat(target.getAttribute("elemy"), 10) - evt.clientY;
  }
}

function mouseUp(evt) {
  if(draggingElement) {
    draggingElement.setAttribute("elemx", evt.clientX + deltax);
    draggingElement.setAttribute("elemy", evt.clientY + deltay);
    if(draggingElement.getAttribute("class") === "key") {
      underelements = document.elementsFromPoint(evt.clientX, evt.clientY);
      for(var element of underelements) {
        if(element.getAttribute("id") === draggingElement.getAttribute("placeid")) {
          keyboard.appendChild(draggingElement);
          draggingElement.setAttribute("onmousedown", "");
          draggingElement.setAttribute("class", "");
          draggingElement.setAttribute("transform", "translate(" + element.getBBox().x + "," + element.getBBox().y + ")");
          break;
        }
      }
    }
  }
  draggingElement = null;
  deltax = null;
  deltay = null;
  elemx = null;
  elemy = null;
}

function mouseMove(evt) {
  if(draggingElement) {
    var curx = (evt.clientX + deltax);
    var cury = (evt.clientY + deltay);
    draggingElement.setAttribute("transform", transformScale(curx, cury));
  }
}

function setParams() {
  var params = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    params[key] = value;
  });
  if(params["scale"]){
    scale = parseFloat(params["scale"]);
    mscale = "matrix(" + scale + ", 0, 0," + scale + ",";
  }
}

function onLoad(){
  var bodyClientRect = document.getElementsByTagName("body")[0].getBoundingClientRect();
  page.width = bodyClientRect.width;
  page.height = bodyClientRect.height;
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "keyboard.svg", true);
  xhr.onload = function(e) {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        keyboard = xhr.responseXML.documentElement.getElementById("whole_keyboard");
        document.getElementById("keypad").appendChild(keyboard);
        draw();
      } else {
        console.error(xhr.statusText);
      }
    }
  };
  xhr.onerror = function (e) {
    console.error(xhr.statusText);
  };
  xhr.overrideMimeType("image/svg+xml");
  xhr.send(null);
}

function draw() {
  svgpad = document.getElementById("svgpad");
  keypad = document.getElementById("keypad");
  keypad.setAttribute("transform", "scale(" + scale + ")")
  var l = 0;
  var c = 0;
  var dl = 10;
  var dc = 10;
  var mainKeys = [].slice.call(document.getElementById("main_keys").getElementsByTagName("path"));
  var keySkin = document.getElementById("key_skin");
  var keycount = mainKeys.length;
  var maxheight = 0;
  while(keycount > 0) {
    var key = mainKeys.splice(randomInt(0, --keycount), 1)[0];
    var keyid = key.getAttribute("id");
    var knode = keySkin.cloneNode(true);
    knode.setAttribute("id", keyid + "_key_skin");
    key = key.cloneNode(true);
    key.setAttribute("id", keyid + "_second");
    knode.insertBefore(key, knode.children[0]);
    svgpad.appendChild(knode);
    knode.setAttribute("id", keyid + "_key");
    knode.setAttribute("placeid", keyid);
    var rect = knode.getElementsByTagName("path")[0];
    var text = knode.getElementsByTagName("text")[0];
    var box = rect.getBBox();
    rect.setAttribute("width", box.width);
    rect.setAttribute("height", box.height);
    rect.setAttribute("style", "fill:#303030");
    rect.setAttribute("transform", "translate(" + (-box.x) + "," + (-box.y) + ")");
    for(tline of keylist[keyid]) {
      tspan = document.createElementNS("http://www.w3.org/2000/svg", "tspan");
      tspan.setAttribute("x", (tline.x !== "") ? tline.x : box.width/2);
      tspan.setAttribute("y", (tline.y !== "") ? tline.y : box.height/2);
      tspan.setAttribute("text-anchor", tline.anchor);
      tspan.setAttribute("font-size", tline.size);
      tspan.innerHTML = tline.text;
      text.appendChild(tspan);
    }
    knode.setAttribute("transform", transformScale(c, l));
    knode.setAttribute("elemx", c);
    knode.setAttribute("elemy", l);
    c += (box.width + dc)*scale;
    if(box.height > maxheight) {
      maxheight = box.height;
    }
    if(c >= 0.9*page.width) {
      c = 0;
      l += (maxheight + dl)*scale;
      maxheight = 0;
    }
  }
  keypad.setAttribute("transform", transformScale(0, l));
  keypad.setAttribute("elemx", 0);
  keypad.setAttribute("elemy", l);
}
